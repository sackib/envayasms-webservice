'use strict';
const app = require('express')();
const port = process.env.PORT || 3000;
const sms = require('./routes/sms');

app.use('/sms', sms);

app.listen(port, () => {
  console.log(`listening on port ${port}`);
});
