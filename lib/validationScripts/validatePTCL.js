'use strict';

/**
 * validatePTCL() validates the sms command given to find ptcl bill by an end user
 * @param {string} smsCommand
 * @return {Error | Array}
 */

function validatePTCL(smsCommand) {
  // trim spaces, turn to lower case and replace multiple spaces with the single one
  const trimmedLowerCaseSMS = smsCommand.trim().toLowerCase().replace(/\s\s+/g, ' ');
  const splittedSMS = trimmedLowerCaseSMS.split(' ');

  if (splittedSMS.length !== 3 || splittedSMS[0] !== 'ptcl') {
    throw new Error('Invalid SMS command, the correct command is \
      ptcl<space>phoneNumber<space>accountID');
  }

  // make sure there are no strings after 'ptcl' in the smsCommand
  for (let i = 0; i < splittedSMS.length; i++) {
    if (i === 0) {
      if (isNaN(splittedSMS[i])) {
        continue;
      }
    }
    if (isNaN(splittedSMS[i])) {
      throw new Error('phoneNumber or accountID cannot be comprised of alphabetic letters or symbols');
    }
  }

  // if phone number contains area code
  if (splittedSMS[1].substr(0, 1) === '0') {
    throw new Error('Please provide the phone number without area code');
  }

  return {
    provider: splittedSMS[0],
    phoneNo: splittedSMS[1],
    accountID: splittedSMS[2],
  };
}

module.exports = validatePTCL;
