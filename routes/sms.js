'use strict';
const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');
const router = new express.Router();
const validatePTCL = require('../lib/validationScripts/validatePTCL');

router.use(bodyParser.urlencoded({
  extended: true,
}));

router.post('/', (req, res) => {
  try {
    const smsCommand = validatePTCL(req.body.message);
    const url = `https://bills-api.herokuapp.com/${smsCommand.provider}?phoneNo=${smsCommand.phoneNo}&accountID=${smsCommand.accountID}`;
    request(url, (error, response, body) => {
      if (!error && response.statusCode == 200) {
        const parsedBody = JSON.parse(body);
        res.json({
          events: [{
            event: 'send',
            messages: [{
              to: req.body.from,
              message: `Dear ${parsedBody.name}, your bill for the billing month of ${parsedBody.billingMonth} is ${parsedBody.billAmount} with due date to be ${parsedBody.dueDate}.`,
            }],
          }],
        });
      } else if (!error && response.statusCode == 404) {
        res.json({
          events: [{
            event: 'send',
            messages: [{
              to: req.body.from,
              message: JSON.parse(body),
            }],
          }],
        });
      } else {
        res.json({
          events: [{
            event: 'send',
            messages: [{
              to: req.body.from,
              message: 'Service temporarily not available, there is some issue with PTCL servers',
            }],
          }],
        });
      }
    });
  } catch (error) {
    res.json({
      events: [{
        event: 'send',
        messages: [{
          to: req.body.from,
          message: `${error.message}`,
        }],
      }],
    });
  }
});

module.exports = router;
